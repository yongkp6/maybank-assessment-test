package com.mbb.assessment.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mbb.assessment.dto.BaseDTO;
import com.mbb.assessment.util.FailableFunction;

import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

// Assessment 4, logging request and response into log file
@Slf4j
public abstract class AbstractController {
	
	private Gson gson;

	@PostConstruct
	public void init() {
        gson = new GsonBuilder().excludeFieldsWithModifiers(java.lang.reflect.Modifier.TRANSIENT).create();
	}

	protected <T extends BaseDTO> T genericRequestResponseService(HttpServletRequest httpRequest, HttpServletResponse httpResponse, 
			T requestDTO, FailableFunction<T, T> serviceFunction) 
			throws Exception {
		
		// Assessment 4, logging request and response into log file
		log.info("Request End Point : {}", httpRequest.getRequestURI());
		log.info("Request Payload : {}", gson.toJson(requestDTO));
		
		T responseDTO = serviceFunction.apply(requestDTO);
		
		if (responseDTO.getResponseHttpStatus() != null) {
			httpResponse.setStatus(responseDTO.getResponseHttpStatus().value());
		}
		
		String responseJson = gson.toJson(responseDTO);
		
		// Assessment 4, logging request and response into log file
		log.info("Response Payload : {}", responseJson);
		
		
		return responseDTO;
	}
}
