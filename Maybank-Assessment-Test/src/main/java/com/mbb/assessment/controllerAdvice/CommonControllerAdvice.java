package com.mbb.assessment.controllerAdvice;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mbb.assessment.dto.ErrorDTO;
import com.mbb.assessment.exception.ForbiddenServiceException;
import com.mbb.assessment.exception.ResourceNotFoundServiceException;

@Slf4j
@ControllerAdvice(basePackages = "com.mbb.assessment.controller")
public class CommonControllerAdvice {

	@ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
	protected @ResponseBody ErrorDTO handleDataIntegrityViolationException(final HttpServletRequest request, final HttpServletResponse response,
			final Exception ex) throws Exception {
		log.error("", ex);
				
		return new ErrorDTO("Resource data has conflict.");
	}
	
	@ExceptionHandler(ResourceNotFoundServiceException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected @ResponseBody ErrorDTO handleResourceNotFoundServiceException(final HttpServletRequest request, final HttpServletResponse response,
                                                                      final ResourceNotFoundServiceException ex) throws Exception {
		log.error("", ex);

        return new ErrorDTO("Resource not found.");
    }
	
	@ExceptionHandler(ForbiddenServiceException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    protected @ResponseBody ErrorDTO handleForbiddenServiceException(final HttpServletRequest request, final HttpServletResponse response,
                                                                      final ForbiddenServiceException ex) throws Exception {
		log.error("", ex);

        return new ErrorDTO("Request is forbidden");
    }
}
