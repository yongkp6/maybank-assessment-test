package com.mbb.assessment.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import com.google.gson.Gson;
import com.mbb.assessment.dao.UserDAO;
import com.mbb.assessment.dto.DataPageDTO;
import com.mbb.assessment.dto.PaginationDTO;
import com.mbb.assessment.dto.UserDTO;
import com.mbb.assessment.entity.User;
import com.mbb.assessment.exception.ForbiddenServiceException;
import com.mbb.assessment.exception.ResourceNotFoundServiceException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserService {
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
    private WebClient.Builder webClientBuilder;
	
	@Value("${url.blacklist}")
	private String blacklistUrl;
	
	@Autowired
	private Gson gson;

	@Transactional
	public UserDTO postCreateUser(UserDTO userDTO) throws ForbiddenServiceException {
		
		// implement idempotency
		User existedUserByApiTransactionId = userDAO.findAllByApiTransactionIdAndApiTransactionIdIsNotNull(userDTO.getApiTransactionId());
		if (existedUserByApiTransactionId != null) {
			log.info("user with apiTransactionId is created before : {} , proceed to return with existing resource data.", userDTO.getApiTransactionId());
			userDTO.setEmail(existedUserByApiTransactionId.getEmail());
			userDTO.setFullName(existedUserByApiTransactionId.getFullName());
			userDTO.setMobileNumber(existedUserByApiTransactionId.getMobileNumber());
			userDTO.setUserId(existedUserByApiTransactionId.getUserId());
			userDTO.setIdentificationNumber(existedUserByApiTransactionId.getIdentificationNumber());
			userDTO.setApiTransactionId(existedUserByApiTransactionId.getApiTransactionId());
			userDTO.setId(existedUserByApiTransactionId.getId());
			return userDTO;
		}
		
		log.info("user with apiTransactionId does not exist : {} , proceed to create new user.", userDTO.getApiTransactionId());
		log.info("hit third party API to check if full name is in blacklist (mock)");
		
		// Assessment 7. Explore an api which will nested calling another api from 3rd party.
		blacklistCheck(userDTO);
		
		User newUser = new User();
		newUser.setEmail(userDTO.getEmail());
		newUser.setFullName(userDTO.getFullName());
		newUser.setMobileNumber(userDTO.getMobileNumber());
		newUser.setUserId(userDTO.getUserId());
		newUser.setIdentificationNumber(userDTO.getIdentificationNumber());
		newUser.setApiTransactionId(userDTO.getApiTransactionId());
		
		userDAO.save(newUser);
		
		userDTO.setId(newUser.getId());
		
		log.info("User is created with User ID : {}", userDTO.getUserId());
		userDTO.setResponseHttpStatus(HttpStatus.CREATED);
		
		return userDTO;
	}

	// Assessment 7. Explore an api which will nested calling another api from 3rd party.
	private void blacklistCheck(UserDTO userDTO) throws ForbiddenServiceException {
		String fullBlacklistUrl;
		try {
			fullBlacklistUrl = blacklistUrl + "?fullName=" + URLEncoder.encode(userDTO.getFullName(), "UTF-8");
			log.info("fullBlacklistUrl : {}", fullBlacklistUrl);
		} catch (UnsupportedEncodingException e) {
			log.error("error encountered when encoding blacklist url");
			log.error("Exception : ", e);
			throw new ForbiddenServiceException();
		}
		
		UserDTO blacklistResponseDTO = webClientBuilder.build().get()
                .uri(fullBlacklistUrl)
                .retrieve()
                .bodyToMono(UserDTO.class)
                .block();
		
		log.info("Blacklist service response payload : {}", gson.toJson(blacklistResponseDTO));
		
		if (Optional.ofNullable(blacklistResponseDTO)
				.map(e -> e.getIsBlacklisted()).orElse(false)) {
			log.error("User full name is blacklisted from blacklist source : {}", userDTO.getFullName());
			throw new ForbiddenServiceException();
		} else {
			log.info("User full name is not blacklisted from blacklist source : {}", userDTO.getFullName());
		}
	}
	
	@Transactional(readOnly = true)
	public DataPageDTO<UserDTO> getUsers(DataPageDTO<UserDTO> dataPageDTO) {
		
		PaginationDTO paginationDTO = dataPageDTO.getPagination();
		Page<User> userList = userDAO.findAll(PageRequest.of(paginationDTO.getOffset().intValue(), paginationDTO.getLimit().intValue()));
		
		List<UserDTO> userDTOList = userList.stream().map(e -> {
			UserDTO u = new UserDTO();
			BeanUtils.copyProperties(e, u);
			return u;
		}).collect(Collectors.toList());
		
		DataPageDTO<UserDTO> returnValue = new DataPageDTO<>();
		returnValue.setData(userDTOList);
		return returnValue;
	}
	
	@Transactional(readOnly = true)
	public UserDTO getUserByUserId(UserDTO userDTO) throws ResourceNotFoundServiceException {
		User user = userDAO.findById(userDTO.getId()).orElse(null);
		
		if (user == null) {
			throw new ResourceNotFoundServiceException();
		}
		
		UserDTO responseDTO = new UserDTO();
		BeanUtils.copyProperties(user, responseDTO);
		return responseDTO;
	}
	
	@Transactional(readOnly = false)
	public UserDTO putUser(UserDTO requestDTO, Long pathVariableId) throws ForbiddenServiceException {
		User user = userDAO.findById(pathVariableId).orElse(null);

		if (user == null) {
			log.info("user with unique ID does not exist : {} , proceed to create new user", pathVariableId);
			postCreateUser(requestDTO);
		} else {
			log.info("user with unique ID is created : {} , proceed to update existing user", pathVariableId);
			user.setEmail(requestDTO.getEmail());
			user.setFullName(requestDTO.getFullName());
			user.setMobileNumber(requestDTO.getMobileNumber());
			user.setUserId(requestDTO.getUserId());
			user.setIdentificationNumber(requestDTO.getIdentificationNumber());
			requestDTO.setApiTransactionId(user.getApiTransactionId());
			requestDTO.setId(user.getId());
		}
		
		return requestDTO;
	}
	
	@Transactional(readOnly = false)
	public UserDTO patchUser(UserDTO requestDTO, Long pathVariableId) throws ResourceNotFoundServiceException {
		User user = userDAO.findById(pathVariableId).orElse(null);

		if (user == null) {
			log.info("user with unique ID does not exist : {}");
			throw new ResourceNotFoundServiceException();
		} 
		
		log.info("user with unique ID is created : {} , proceed to update existing user", pathVariableId);
		
		if (requestDTO.getFullName() != null) {
			user.setFullName(requestDTO.getFullName());
		}
		
		if (requestDTO.getEmail() != null) {
			user.setEmail(requestDTO.getEmail());
		}
		
		if (requestDTO.getIdentificationNumber() != null) {
			user.setIdentificationNumber(requestDTO.getIdentificationNumber());
		}
		
		if (requestDTO.getMobileNumber() != null) {
			user.setMobileNumber(requestDTO.getMobileNumber());
		}
		
		if (requestDTO.getUserId() != null) {
			user.setUserId(requestDTO.getUserId());
		}
		
		BeanUtils.copyProperties(user, requestDTO);
		
		return requestDTO;
	}
	
}
