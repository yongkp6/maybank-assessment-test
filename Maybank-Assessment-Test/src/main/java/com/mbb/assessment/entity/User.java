package com.mbb.assessment.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class User extends BasicTable {
	
	private static final long serialVersionUID = 1L;

	@Column(length = 100, unique = true)
	private String userId;
	
	@Column(length = 100)
	private String fullName;
	
	@Column(length = 100)
	private String email;
	
	@Column(length = 20)
	private String mobileNumber;
	
	@Column(length = 50)
	private String identificationNumber;
	
	@Column(unique = true)
	private String apiTransactionId;
}
