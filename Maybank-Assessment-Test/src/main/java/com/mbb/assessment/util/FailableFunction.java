package com.mbb.assessment.util;

public interface FailableFunction<T, R> {
    R apply(T t) throws Exception;

    static <T> FailableFunction<T, T> identity() {
        return t -> t;
    }
}

