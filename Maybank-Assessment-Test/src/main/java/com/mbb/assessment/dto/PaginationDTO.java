package com.mbb.assessment.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaginationDTO {
	private Long offset;
	private Long limit;
	private Long dataCount;
}
