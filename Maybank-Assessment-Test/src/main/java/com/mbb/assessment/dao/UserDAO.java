package com.mbb.assessment.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mbb.assessment.entity.User;

public interface UserDAO extends JpaRepository<User, Long> {
	
    User findAllByUserId(String userId);
    
    User findAllByApiTransactionIdAndApiTransactionIdIsNotNull(String apiTransactionId);

}
