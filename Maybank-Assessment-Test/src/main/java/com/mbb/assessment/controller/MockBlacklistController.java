package com.mbb.assessment.controller;

import java.util.Random;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mbb.assessment.dto.UserDTO;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/users")
public class MockBlacklistController {

	@GetMapping("/blacklist-check")
	public UserDTO getUserBlacklistStatus(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
    		@RequestParam(required = true) String fullName) throws Exception {
		
		log.info("HERE");
		log.info("Full Name : {}", fullName);
        
        Random random = new Random();
        Boolean isBlacklisted = random.nextBoolean();
        
        UserDTO responseDTO = new UserDTO();
        responseDTO.setFullName(fullName);
        responseDTO.setIsBlacklisted(isBlacklisted);
        return responseDTO;
    }
}
