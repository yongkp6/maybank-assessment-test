package com.mbb.assessment.dto;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
public class SortDTO extends BaseDTO{
	
	private String field;
	private String order;
}
