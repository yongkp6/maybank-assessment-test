package com.mbb.assessment.controller;

import com.mbb.assessment.dto.DataPageDTO;
import com.mbb.assessment.dto.UserDTO;
import com.mbb.assessment.service.UserService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
public class UserController extends AbstractController {

    @Autowired
    private UserService userService;

    @PostMapping
    public UserDTO createUser(@RequestBody final UserDTO requestDTO, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws Exception {
        return super.genericRequestResponseService(httpRequest, httpResponse, requestDTO, userService::postCreateUser);
    }
    
    // Assessment 6, GET method with pagination (default to 10 records per page)
    @GetMapping
    public DataPageDTO<UserDTO> getUsers(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
    		@RequestParam(required = false) Long offset,
    		@RequestParam(required = false) Long limit) throws Exception {
		DataPageDTO<UserDTO> requestDTO = new DataPageDTO<>(offset, limit);
        return super.genericRequestResponseService(httpRequest, httpResponse, requestDTO, userService::getUsers);
    }
    
    @GetMapping("/{id}")
    public UserDTO getUserById(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
    		@PathVariable(required = true) Long id) throws Exception {
    	UserDTO requestDTO = new UserDTO(id);
        return super.genericRequestResponseService(httpRequest, httpResponse, requestDTO, userService::getUserByUserId);
    }
    
    @PutMapping("/{id}")
    public UserDTO putUser(@RequestBody final UserDTO requestDTO, HttpServletRequest httpRequest, HttpServletResponse httpResponse,
    		@PathVariable(required = true) Long id) throws Exception {
        return super.genericRequestResponseService(httpRequest, httpResponse, requestDTO, reqDTO -> userService.putUser(requestDTO, id));
    }
    
    @PatchMapping("/{id}")
    public UserDTO patchUser(@RequestBody final UserDTO requestDTO, HttpServletRequest httpRequest, HttpServletResponse httpResponse,
    		@PathVariable(required = true) Long id) throws Exception {
        return super.genericRequestResponseService(httpRequest, httpResponse, requestDTO, reqDTO -> userService.patchUser(requestDTO, id));
    }
}
