package com.mbb.assessment.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
@NoArgsConstructor
public class UserDTO extends BaseDTO {


	private String userId;
	private String fullName;
	private String email;
	private String mobileNumber;
	private String identificationNumber;
	private Boolean isBlacklisted;
	
	public UserDTO(Long id) {
		super.setId(id);
	}
}
