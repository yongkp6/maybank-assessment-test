package com.mbb.assessment.dto;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseDTO implements Serializable {
	private static final transient long serialVersionUID = 7206732992462048194L;
	
	private Long id;
	
	@JsonIgnore
	private transient HttpStatus responseHttpStatus;
	
	private String apiTransactionId;
	
}
