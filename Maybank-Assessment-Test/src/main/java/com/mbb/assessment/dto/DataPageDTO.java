package com.mbb.assessment.dto;

import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@SuppressWarnings("serial")
@Getter
@Setter
@NoArgsConstructor
public class DataPageDTO<T> extends BaseDTO{
	
	private HashMap<String, String> filter;
	private List<T> data;
	private PaginationDTO pagination;
	private SortDTO sort;
	
	public DataPageDTO(List<T> data) {
        this.data = data;
    }

	public DataPageDTO(Long offset, Long limit) {
		PaginationDTO pagination = new PaginationDTO();
		pagination.setOffset(offset != null ? offset : 0L);
		pagination.setLimit(limit != null ? limit : 10L);
		this.pagination = pagination;

		this.filter = new HashMap<>();
	}

	public DataPageDTO(Long offset, Long limit, String sort, String order) {
		PaginationDTO pagination = new PaginationDTO();
		pagination.setOffset(offset);
		pagination.setLimit(limit);
		this.pagination = pagination;

		SortDTO sortDTO = new SortDTO();
		sortDTO.setField(sort);
		sortDTO.setOrder(order);
		this.sort = sortDTO;

		this.filter = new HashMap<>();
	}
}
